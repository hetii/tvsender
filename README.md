# TV sender #

### What is this repository for? ###

* TV transmitter based on Stefan0719 design.

### How do I get set up? ###

*Please check: https://www.youtube.com/watch?v=_rByTxIgLt8

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### RESULT :) ###
![IMG_tvsender_circuit.png](https://bytebucket.org/hetii/tvsender/raw/ef0bcd92e725dba5f3bfba0592c0723d5823f6f9/tht/img/tvsender_circuit.png)
![IMG_tvsender.png](https://bytebucket.org/hetii/tvsender/raw/b46fdf5d8c33d34f894ea846d776d2cef3f66aab/tht/img/tvsender.png)
![IMG_tvsender_bottom.png](https://bytebucket.org/hetii/tvsender/raw/121224f53d5a5241b72bf0ca69ec49a276fff66d/tht/img/tvsender_bottom.png)
![IMG_tvsender_assembly_top.jpg](https://bytebucket.org/hetii/tvsender/raw/ebcde618cbe210a5b067f6d143598ebb51f2d01c/tht/img/tvsender_assembly_top.jpg)
![IMG_tvsender_assembly_bottom.jpg](https://bytebucket.org/hetii/tvsender/raw/ebcde618cbe210a5b067f6d143598ebb51f2d01c/tht/img/tvsender_assembly_bottom.jpg)